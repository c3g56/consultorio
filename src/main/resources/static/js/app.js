console.log("Iniciando js.");
const url = 'http://localhost:8080/api/especialidades/'; //define la url para consumir el servicio
//se obtienen los valores de entrada
const formulario = document.getElementById('formulario');
const nombre = document.getElementById('nombre');
const imagen = document.getElementById('imagen');
const listado = document.getElementById('listado');

const idespecialidad = document.getElementById('idespecialidad');
const formularioEditar = document.getElementById('formularioEdit');
const nombreEdit = document.getElementById('nombreedit');

if(formularioEditar !=null){
    formularioEditar.addEventListener('submit',(e)=>{
        e.preventDefault();
        let especialidad = {
            id:parseInt(idespecialidad.textContent),
            nombre:nombreEdit.value,
            estado:true
           };
           console.log(especialidad);

           const headers = {
            method:"PUT",
            headers:{
             accept:"application.json",
             "Content-Type":'application/json'
            },
            body:JSON.stringify(especialidad)
           }
           fetch(url+'actualizar',headers)
           .then(respuesta =>{
            console.log(respuesta);
            if(respuesta.ok){
                location.href = "http://localhost:8080/especialidades";
            }
           })
    })
}






if(idespecialidad){
    console.log(idespecialidad);
    obtenerPorId(idespecialidad.textContent);
}
//se agrega el evento para manejar el formulario
if(formulario != null){
    formulario.addEventListener('submit',(e)=>{
        e.preventDefault();
       
        //construir el objecto que se va a enviar atraves de una peticion post
       let especialidad = {
        nombre:nombre.value,
        imagen:imagen.value,
        estado:true
       };
       //enviar la peticion
       console.log(especialidad);
       const headers = {
        method:"POST",
        headers:{
         accept:"application.json",
         "Content-Type":'application/json'
        },
        body:JSON.stringify(especialidad)
       }
       fetch(url+'registrar',headers)
       .then(respuesta =>{
        console.log(respuesta);
        if(respuesta.ok){
            location.href = "http://localhost:8080/especialidades";
        }
       })
    })
}

function listarDatos(){
    if(listado){
        let plantilla='';
        fetch(url+'listar')
        .then(async(resp)=>{
            let especialidades = await resp.json();
            especialidades.forEach(element => {
                plantilla+=`
                <tr espid="${element.id}">
                    <th scope="row">
                        <a  href="/especialidades/editar/${element.id}" class="btn btn-warning"> Editar</a>
                        <a  class="btn btn-danger eliminar"> Eliminar</a>
                    </th>
                    <td>${element.id}</td>
                    <td><img class="img" src="${element.imagen}" /></td>
                    <td>${element.nombre}</td>
                    <td>${element.estado}</td>
                </tr>
                `;
            });
            listado.innerHTML = plantilla;
        })
    }
 
}

function obtenerPorId(id){
    let plantilla='';
       fetch(url+'obtenerPorId/'+id)
       .then(async(resp)=>{
        let especialidad = await resp.json();
        console.log(especialidad);
        console.log(nombreEdit);
        nombreEdit.value = especialidad.nombre;
       })
   }

function addEventDelete(){
    //obtenemos todos los botones
    const btnEliminar = document.querySelectorAll(".eliminar");
    console.log(btnEliminar);
    btnEliminar.forEach(btn=>{
        btn.addEventListener('click',(e)=>{
            let fila = btn.parentElement.parentElement;
            let id = fila.getAttribute('espid');
            let estado = confirm('Desea eliminar el registro:'+id);
            if(estado){
                //aqui se envia la peticion htpp al servidor
                // se configura la cabecera de la peticion
                const headers = {
                    method:"DELETE",
                    headers:{
                     accept:"application.json",
                     "Content-Type":'application/json'
                    }
                   }
                fetch(url+'eliminar/'+id,headers)
                .then(respuesta=>{
                    if(respuesta.ok){
                        init();

                    }
                })
            }
        })
    })
    
}
function init(){
    listarDatos();
    setTimeout(()=>{
        addEventDelete();
    },500);
}

init();

