package com.consultorio.app.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.consultorio.app.entities.Especialidad;
import com.consultorio.app.servicios.EspecialidaService;



@RestController
@RequestMapping("/api/especialidades")
public class EspecialidadControllerApi {
	
	@Autowired
	EspecialidaService especialidaService;
	
	//Esta ruta permite listar todas las especialidades
	@GetMapping("/listar")
	List<Especialidad> listar() {
		return especialidaService.listar();
	}
	
	@GetMapping("/obtenerPorId/{id}")
	Especialidad obtenerPorId(@PathVariable("id") Long id) {
		return especialidaService.obtenerPorId(id);
	}
	
	@PostMapping("/registrar")
	void registrar(@RequestBody Especialidad especialidad) {
		especialidaService.guardar(especialidad);
	}
	
	@PutMapping("/actualizar")
	void actualizar(@RequestBody Especialidad especialidad) {
		Especialidad existeEspecialidad = especialidaService.obtenerPorId(especialidad.getId());
		if(existeEspecialidad != null) {
			especialidaService.guardar(especialidad);			
		}
	}
	
	@DeleteMapping("/eliminar/{id}")
	void eliminar(@PathVariable("id") Long id) {
		especialidaService.eliminar(id);
	}
	


}
