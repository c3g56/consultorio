package com.consultorio.app.repositorios;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.consultorio.app.entities.Especialidad;

@Repository
public interface EspecialidadRepository extends CrudRepository<Especialidad, Long>{

}
