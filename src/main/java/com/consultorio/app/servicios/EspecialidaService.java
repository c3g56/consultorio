package com.consultorio.app.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.consultorio.app.entities.Especialidad;
import com.consultorio.app.repositorios.EspecialidadRepository;

@Service
public class EspecialidaService {
	
	@Autowired
	EspecialidadRepository  especialidadRepository;
	
	public List<Especialidad> listar(){
		return (List<Especialidad>) especialidadRepository.findAll();
	}
	
	
	//metodo para obtener un registro
	
	public Especialidad obtenerPorId(Long id) {
		return especialidadRepository.findById(id).orElse(null);
	}
	
	//metodo para guardarlo
	
	public void guardar(Especialidad especialidad) {
		especialidadRepository.save(especialidad);
	}
	
	
	//metodo para eliminar
	
	public void eliminar(Long id) {
		Especialidad especialidad = obtenerPorId(id);
		especialidadRepository.delete(especialidad);
	}

}
