package com.consultorio.app.controladores;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/medicos")
public class MedicoControlador {

	
	@GetMapping(value = {"/index",""})
	public String index(Model model) {
		model.addAttribute("titulo", "Listado de medicos");
		model.addAttribute("ejemplo", "ejemplo");
		return "medicos/index";
	}
	
	@GetMapping("/agregar")
	public String agregar(Model model) {
		model.addAttribute("titulo", "Registrar medico");
		
		return "medicos/crear";
	}
}
