package com.consultorio.app.controladores;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/especialidades")
public class EspecialidadController {
	
	
	@GetMapping(value = {"/index",""})
	public String index(Model model) {
		model.addAttribute("titulo", "Listado de especialidades");
		
		return "especialidades/index";
	}
	
	
	@GetMapping("/agregar")
	public String agregar(Model model) {
		model.addAttribute("titulo", "Registrar especialidad");
		
		return "especialidades/crear";
	}
	
	
	@GetMapping("/editar/{id}")
	public String editar(Model model, @PathVariable("id") Long id) {
		model.addAttribute("titulo", "Registrar especialidad");
		model.addAttribute("id", id);
		return "especialidades/editar";
	}
	
	@GetMapping("/ejemplo")
	public String ejemplo(Model model) {
		model.addAttribute("titulo", "Registrar especialidad");
	
		return "ejemplo";
	}

}
